package org.coralpoop.infrastructure.adapter.repository;

import com.google.common.cache.Cache;
import org.coralpoop.domain.game.adapter.repository.IInitGameCacheRepository;
import org.coralpoop.domain.game.model.entity.PlayerEntity;
import org.coralpoop.domain.game.model.entity.PokerEntity;
import org.coralpoop.types.common.Constants;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;

/**
 * @Title: InitGameCacheRepository.java
 * @Package: org.coralpoop.infrastructure.adapter.repository
 * @Description: 数据容器缓存仓储实现
 * @Author: 006081
 * @Date: 2025/1/22 14:39
 * @Version: V1.0
 */
@Repository
public class InitGameCacheRepository implements IInitGameCacheRepository {
    @Resource(name = "pokerCache")
    private Cache<String, Map<String, PokerEntity>> pokerCache;

    @Resource(name = "playerCache")
    private Cache<String, Map<String, PlayerEntity>> playerCache;

    @Resource(name = "handPokerCache")
    private Cache<String, Map<String, Set<PokerEntity>>> handPokerCache;

    @Resource(name = "publicPokerCache")
    private Cache<String, Set<PokerEntity>> publicPokerCache;

    @Resource(name = "betPoolCache")
    private Cache<String, Map<Integer,Map<String,Long>>> betPoolCache;

    @Override
    public Boolean pokerCachePut(String uuid, Map<String, PokerEntity> pokerMap) {
        pokerCache.put(Constants.POKER_PRE + uuid , pokerMap);
        return true;
    }

    @Override
    public Map<String, PokerEntity> pokerCacheGet(String uuid) {
        return pokerCache.getIfPresent(Constants.POKER_PRE + uuid);
    }

    @Override
    public Boolean playerCachePut(String uuid, Map<String, PlayerEntity> playerMap) {
        playerCache.put(Constants.PLAYER_PRE + uuid,playerMap);
        return true;
    }

    @Override
    public Map<String, PlayerEntity> playerCacheGet(String uuid) {
        return playerCache.getIfPresent(Constants.PLAYER_PRE + uuid);
    }

    @Override
    public Boolean handPokerCachePut(String uuid, Map<String, Set<PokerEntity>> handPokerMap) {
        handPokerCache.put(Constants.HAND_POKER_PRE + uuid,handPokerMap);
        return true;
    }

    @Override
    public Map<String, Set<PokerEntity>> handPokerCacheGet(String uuid) {
        return handPokerCache.getIfPresent(Constants.HAND_POKER_PRE + uuid);
    }

    @Override
    public Boolean publicPokerCachePut(String uuid, Set<PokerEntity> publicPoker) {
        publicPokerCache.put(Constants.PUBLIC_POKER_PRE + uuid,publicPoker);
        return true;
    }

    @Override
    public Set<PokerEntity> publicPokerCacheGet(String uuid) {
        return publicPokerCache.getIfPresent(Constants.PUBLIC_POKER_PRE + uuid);
    }

    @Override
    public Boolean betPoolCachePut(String uuid, Map<Integer, Map<String, Long>> betPoolMap) {
        betPoolCache.put(Constants.BET_POOL_PRE + uuid, betPoolMap);
        return true;
    }

    @Override
    public Map<Integer, Map<String, Long>> betPoolCacheGet(String uuid) {
        return betPoolCache.getIfPresent(Constants.BET_POOL_PRE + uuid);
    }
}
