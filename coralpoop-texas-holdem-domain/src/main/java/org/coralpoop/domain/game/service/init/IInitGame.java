package org.coralpoop.domain.game.service.init;

/**
 * @Title: IInitGame.java
 * @Package: org.coralpoop.domain.game.service.init
 * @Description: 初始化容器接口，只负责容器的刷盘和基础牌堆的装配
 * @Author: 006081
 * @Date: 2025/1/22 09:50
 * @Version: V1.0
 */
public interface IInitGame {
    Boolean initPoker(String uuid);

    Boolean initPlayer(String uuid, String playerToken, Long money);

    Boolean initHandPoker(String uuid);

    Boolean initPublicPoker(String uuid);

    Boolean initBetPool(String uuid);
}
