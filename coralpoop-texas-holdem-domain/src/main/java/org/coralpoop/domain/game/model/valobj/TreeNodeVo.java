package org.coralpoop.domain.game.model.valobj;

import lombok.Data;

/**
 * @Title: TreeNodeVo.java
 * @Package: org.coralpoop.domain.game.model.valobj
 * @Description: 规则树节点
 * @Author: 006081
 * @Date: 2025/1/23 14:41
 * @Version: V1.0
 */
@Data
public class TreeNodeVo {
    /**uuid*/
    private String uuid;

    /**节点名称*/
    private String nodeName;

    /**左孩子*/
    private TreeNodeVo leftChild;

    /**右孩子*/
    private TreeNodeVo rightChild;
}
