package org.coralpoop.domain.game.model.entity;

import lombok.Data;

/**
 * @Title: PokerEntity.java
 * @Package: org.coralpoop.domain.game.model.entity
 * @Description: 扑克牌实体
 * @Author: 006081
 * @Date: 2025/1/22 09:35
 * @Version: V1.0
 */
@Data
public class PokerEntity {
    /**id*/
    private String id;

    /**点数*/
    private Integer point;

    /**花色(1=方片 2=梅花 3=红桃 4=黑桃)*/
    private Integer suit;

    public String getId() {
        return suit.toString() + point.toString();
    }
}
