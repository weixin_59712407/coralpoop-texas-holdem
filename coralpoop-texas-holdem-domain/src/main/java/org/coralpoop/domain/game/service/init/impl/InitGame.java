package org.coralpoop.domain.game.service.init.impl;

import lombok.extern.slf4j.Slf4j;
import org.coralpoop.domain.game.adapter.repository.IInitGameCacheRepository;
import org.coralpoop.domain.game.model.entity.PlayerEntity;
import org.coralpoop.domain.game.model.entity.PokerEntity;
import org.coralpoop.domain.game.service.init.IInitGame;
import org.coralpoop.types.common.Constants;
import org.coralpoop.types.enums.RoundEnum;
import org.coralpoop.types.exception.AppException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Title: InitGame.java
 * @Package: org.coralpoop.domain.game.service.init.impl
 * @Description: 初始化容器实现
 * @Author: 006081
 * @Date: 2025/1/22 09:50
 * @Version: V1.0
 */
@Slf4j
@Service
public class InitGame implements IInitGame {
    @Resource
    private IInitGameCacheRepository initGameCacheRepository;

    @Override
    public Boolean initPoker(String uuid) {
        Map<String, PokerEntity> pokerMap = new ConcurrentHashMap<>();
        for (int i = 1; i <= 4; i++) {
            for (int j = 1; j <= 13; j++) {
                PokerEntity poker = new PokerEntity();
                poker.setSuit(i);
                poker.setPoint(j);
                pokerMap.put(poker.getId(), poker);
            }
        }
        try {
            //只要调用就清空原来牌库
            return initGameCacheRepository.pokerCachePut(uuid, pokerMap);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean initPlayer(String uuid, String playerToken, Long money) {
        //玩家信息
        PlayerEntity player = new PlayerEntity();
        player.setPlayerToken(playerToken);
        player.setMoney(money);
        try {
            //玩家容器需校验原来是否已存在，因为中途会加入
            Map<String, PlayerEntity> playerMap = initGameCacheRepository.playerCacheGet(uuid);
            //没有容器则创建
            if (Objects.isNull(playerMap)) {
                Map<String, PlayerEntity> newMap = new ConcurrentHashMap<>();
                newMap.put(playerToken, player);
                return initGameCacheRepository.playerCachePut(uuid,newMap);
            }
            playerMap.put(playerToken, player);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean initHandPoker(String uuid) {
        try {
            Map<String, PlayerEntity> playerMap = initGameCacheRepository.playerCacheGet(uuid);
            if (CollectionUtils.isEmpty(playerMap)) {
                throw new AppException("局内无玩家");
            }
            Map<String, Set<PokerEntity>> handPokerMap = new ConcurrentHashMap<>();
            //给每个玩家创建一个底牌容器
            for (Map.Entry<String, PlayerEntity> entry : playerMap.entrySet()) {
                handPokerMap.put(entry.getKey(), new HashSet<>());
            }
            initGameCacheRepository.handPokerCachePut(uuid, handPokerMap);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean initPublicPoker(String uuid) {
        try {
            Set<PokerEntity> publicPokerMap = new HashSet<>();
            initGameCacheRepository.publicPokerCachePut(uuid, publicPokerMap);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean initBetPool(String uuid) {
        try {
            Map<String, PlayerEntity> playerMap = initGameCacheRepository.playerCacheGet(uuid);
            if (CollectionUtils.isEmpty(playerMap)) {
                throw new AppException("局内无玩家");
            }
            Map<Integer,Map<String,Long>> betPoolMap = new ConcurrentHashMap<>();
            for(int i = RoundEnum.ONE.getCode();i<=RoundEnum.FOUR.getCode();i++){
                Map<String, Long> map = new ConcurrentHashMap<>();
                for(Map.Entry<String, PlayerEntity> entry : playerMap.entrySet()){
                    map.put(entry.getKey(), 0L);
                }
                betPoolMap.put(i,map);
            }
            initGameCacheRepository.betPoolCachePut(uuid,betPoolMap);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
