package org.coralpoop.domain.game.service.settle.tree;

import org.coralpoop.domain.game.service.settle.tree.factory.DefaultTreeFactory;

/**
 * @Title: ILogicTreeNode.java
 * @Package: org.coralpoop.domain.game.service.settle.tree
 * @Description: 规则树节点
 * @Author: 006081
 * @Date: 2025/1/22 15:56
 * @Version: V1.0
 */
public interface ILogicTreeNode {
    DefaultTreeFactory.TreeActionEntity logic(String uuid);
}
