package org.coralpoop.domain.game.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Title: TreeCheckTypeVo.java
 * @Package: org.coralpoop.domain.game.model.valobj
 * @Description: 规则树动作枚举
 * @Author: 006081
 * @Date: 2025/1/22 17:50
 * @Version: V1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum TreeCheckTypeVo {
    ALLOW(1,"放行:执行后续流程"),
    TAKE_OVER(2,"接管:执行节点逻辑"),
    ;

    private Integer code;
    private String info;
}
