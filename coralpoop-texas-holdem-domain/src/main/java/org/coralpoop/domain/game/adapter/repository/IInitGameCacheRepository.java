package org.coralpoop.domain.game.adapter.repository;

import org.coralpoop.domain.game.model.entity.PlayerEntity;
import org.coralpoop.domain.game.model.entity.PokerEntity;

import java.util.Map;
import java.util.Set;

/**
 * @Title: IInitGameCacheRepository.java
 * @Package: org.coralpoop.domain.game.adapter.repository
 * @Description: 数据容器缓存仓储接口
 * @Author: 006081
 * @Date: 2025/1/22 14:30
 * @Version: V1.0
 */
public interface IInitGameCacheRepository {
    Boolean pokerCachePut(String uuid, Map<String, PokerEntity> pokerMap);
    Map<String, PokerEntity> pokerCacheGet(String uuid);

    Boolean playerCachePut(String uuid, Map<String, PlayerEntity> playerMap);
    Map<String, PlayerEntity> playerCacheGet(String uuid);

    Boolean handPokerCachePut(String uuid, Map<String, Set<PokerEntity>> handPokerMap);
    Map<String, Set<PokerEntity>> handPokerCacheGet(String uuid);

    Boolean publicPokerCachePut(String uuid, Set<PokerEntity> publicPoker);
    Set<PokerEntity> publicPokerCacheGet(String uuid);

    Boolean betPoolCachePut(String uuid, Map<Integer,Map<String,Long>> betPoolMap);
    Map<Integer,Map<String,Long>> betPoolCacheGet(String uuid);
}
