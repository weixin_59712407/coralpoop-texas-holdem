package org.coralpoop.domain.game.model.entity;

import lombok.Data;

/**
 * @Title: PlayerEntity.java
 * @Package: org.coralpoop.domain.game.model.entity
 * @Description: 玩家实体
 * @Author: 006081
 * @Date: 2025/1/22 10:34
 * @Version: V1.0
 */
@Data
public class PlayerEntity {
    /**唯一标识*/
    private String playerToken;

    /**身份*/
    private Integer identity = 0;

    /**点数*/
    private Long money;
}
