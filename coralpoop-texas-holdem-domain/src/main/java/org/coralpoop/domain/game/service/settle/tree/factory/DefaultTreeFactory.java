package org.coralpoop.domain.game.service.settle.tree.factory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.coralpoop.domain.game.model.valobj.TreeCheckTypeVo;
import org.coralpoop.domain.game.service.settle.tree.factory.engine.IDecisionTreeEngine;
import org.coralpoop.domain.game.service.settle.tree.factory.engine.impl.DecisionTreeEngine;
import org.springframework.stereotype.Service;

/**
 * @Title: DefaultTreeFactory.java
 * @Package: org.coralpoop.domain.game.service.settle.tree.factory
 * @Description: 规则树装配工厂，创建规则树引擎
 * @Author: 006081
 * @Date: 2025/1/22 15:01
 * @Version: V1.0
 */
@Service
public class DefaultTreeFactory {
    public IDecisionTreeEngine openLogicTree(){
        return new DecisionTreeEngine();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class TreeActionEntity{
        /**节点结算结果*/
        private TreeCheckTypeVo checkType;

        /**玩家唯一标识*/
        private String playerToken;

        /**结算点数*/
        private Long money;
    }
}
