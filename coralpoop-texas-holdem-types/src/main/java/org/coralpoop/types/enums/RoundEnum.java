package org.coralpoop.types.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Title: RoundEnum.java
 * @Package: org.coralpoop.types.enums
 * @Description: 对局轮次枚举
 * @Author: 006081
 * @Date: 2025/1/22 14:16
 * @Version: V1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum RoundEnum {
    ONE(1,"番前下注"),
    TWO(2,"翻开三张后下注"),
    THREE(3,"翻开第四张后下注"),
    FOUR(4,"翻开第五张后下注"),
    ;

    private Integer code;
    private String desc;
}
