package org.coralpoop.types.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Title: IdentityEnum.java
 * @Package: org.coralpoop.types.enums
 * @Description: 身份枚举
 * @Author: 006081
 * @Date: 2025/1/22 10:31
 * @Version: V1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum IdentityEnum {
    NONE(0,"无身份"),
    DEALER(1,"庄家"),
    LITTLE_BLIND(2,"小盲"),
    BIG_BLIND(3,"大盲"),
    ;

    private Integer code;

    private String name;
}
