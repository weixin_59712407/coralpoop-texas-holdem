package org.coralpoop.types.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Title: SuitEnum.java
 * @Package: org.coralpoop.types.enums
 * @Description: 花色枚举
 * @Author: 006081
 * @Date: 2025/1/22 09:40
 * @Version: V1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum SuitEnum {
    DIAMONDS(1,"方片"),
    CLUBS(2,"梅花"),
    HEARTS(3,"红桃"),
    SPADES(4,"黑桃"),
    ;

    private Integer code;
    private String name;
}
