package org.coralpoop.types.common;

public class Constants {
    public static final String SPLIT = ",";
    public static final String COLON = ":";
    public static final String SPACE = " ";
    public static final String UNDERLINE = "_";
    public static final String BLANK = "";
    public static final String SIGN = "-";

    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer THREE = 3;
    public static final Integer FOUR = 4;
    public static final Integer FIVE = 5;
    public static final Integer SIX = 6;

    public static final String ZERO_ZERO = "00";
    public static final String NINE_NINE = "99";
    public static final String ZERO_ZERO_ZERO = "000";
    public static final String NINE_NINE_NINE = "999";

    public static final String POKER_PRE = "poker:";
    public static final String PLAYER_PRE = "player:";
    public static final String HAND_POKER_PRE = "handPoker:";
    public static final String PUBLIC_POKER_PRE = "publicPoker:";
    public static final String BET_POOL_PRE = "betPool:";
}
