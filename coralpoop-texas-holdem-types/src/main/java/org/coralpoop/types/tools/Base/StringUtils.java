package org.coralpoop.types.tools.Base;

/**
 * @Title: StringUtils.java
 * @Package: org.coralpoop.types.tools.Base
 * @Description: String工具类
 * @Author: 006081
 * @Date: 2024/11/29 11:52
 * @Version: V1.0
 */
public class StringUtils {
    /**
     * 判断字符串是否为空或空串
     *
     * @param str 目标字符串
     * @return boolean
     * @author 006081
     */
    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 判断字符串是否不为空或空串
     *
     * @param str 目标字符串
     * @return boolean
     * @author 006081
     */
    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }
}
