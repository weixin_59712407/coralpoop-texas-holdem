package org.coralpoop.types.tools.Base;

import lombok.SneakyThrows;

import java.lang.reflect.Field;

/**
 * @Title: ObjectUtils.java
 * @Package: org.coralpoop.types.tools.Base
 * @Description: Object工具类
 * @Author: 006081
 * @Date: 2024/12/6 16:39
 * @Version: V1.0
 */
public class ObjectUtils {
    public static boolean isNull(Object obj) {
        return obj instanceof String ? StringUtils.isBlank(obj.toString()) : obj == null;
    }

    public static boolean nonNull(Object obj) {
        return obj != null;
    }

    @SneakyThrows
    public static boolean isNullOrAnyFieldIsNull(Object obj) {
        if (isNull(obj)) {
            return true;
        }
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            Object o = field.get(obj);
            return o instanceof String ? StringUtils.isBlank(o.toString()) : isNull(o);
        }
        return false;
    }

    public static boolean allFieldNonNull(Object obj) {
        return !isNullOrAnyFieldIsNull(obj);
    }
}
