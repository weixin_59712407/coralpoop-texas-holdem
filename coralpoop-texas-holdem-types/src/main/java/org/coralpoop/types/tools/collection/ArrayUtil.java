package org.coralpoop.types.tools.collection;

import java.lang.reflect.Array;

/**
 * @Title: ArrayUtil.java
 * @Package: org.coralpoop.tools.collection
 * @Description: 数组工具类
 * @Author: 006081
 * @Date: 2024/11/28 17:52
 * @Version: V1.0
 */
public class ArrayUtil {
    /**
     * 传入类型与大小创建数组
     *
     * @param type   目标数组的类型
     * @param length 数组长度
     * @apiNote Array.newInstance()的性能并不差
     * @author 006081
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] newArray(Class<T> type, int length) {
        return (T[]) Array.newInstance(type, length);
    }
}
