package org.coralpoop.types.tools.mapper;

import org.coralpoop.types.tools.collection.ArrayUtil;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title: BeanMapper.java
 * @Package: org.coralpoop.tools.mapper
 * @Description: 映射工具
 * @Author: 006081
 * @Date: 2024/11/28 17:49
 * @Version: V1.0
 */
public class BeanMapper {

    private static Mapper mapper = new DozerBeanMapper();

    /**
     * 简单的复制出新类型对象
     *
     * @param source           复制的目标
     * @param destinationClass 要转换的类型
     * @return 复制的结果
     * @author 006081
     */
    public static <S, D> D map(S source, Class<D> destinationClass) {
        if (source == null) {
            return null;
        }
        return mapper.map(source, destinationClass);
    }

    /**
     * 简单的复制出新对象ArrayList
     *
     * @param sourceList       复制的目标集合
     * @param destinationClass 要转换的类型
     * @return 复制的结果
     * @author 006081
     */
    public static <S, D> List<D> mapList(Iterable<S> sourceList, Class<D> destinationClass) {
        if (null == sourceList) {
            return null;
        }
        List<D> destinationList = new ArrayList<D>();
        for (S source : sourceList) {
            if (source != null) {
                destinationList.add(mapper.map(source, destinationClass));
            }
        }
        return destinationList;
    }

    /**
     * 简单复制出新对象数组
     *
     * @param sourceArray      复制的目标数组
     * @param destinationClass 要转换的类型
     * @return 复制的结果
     * @author 006081
     */
    public static <S, D> D[] mapArray(final S[] sourceArray, final Class<D> destinationClass) {
        D[] destinationArray = ArrayUtil.newArray(destinationClass, sourceArray.length);

        int i = 0;
        for (S source : sourceArray) {
            if (source != null) {
                destinationArray[i] = mapper.map(sourceArray[i], destinationClass);
                i++;
            }
        }

        return destinationArray;
    }
}