package org.coralpoop.types.tools.Base;

import lombok.SneakyThrows;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @Title: EnumUtils.java
 * @Package: org.coralpoop.types.tools.Base
 * @Description: 枚举类工具类
 * @Author: 006081
 * @Date: 2024/12/3 10:28
 * @Version: V1.0
 */
public class EnumUtils {
    private final static String VALUES = "values";
    private final static String GET = "get";

    /**
     * 根据值获取枚举
     *
     * @param byWhat  用于获取枚举的值
     * @param clazz   目标枚举类
     * @param getWhat 用于获取枚举的值的名字(首字母大写)
     * @return 目标枚举
     * @apiNote 此方法使用SneakyThrows，实际会抛出InvocationTargetException、NoSuchMethodException、IllegalAccessException
     * @author 006081
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T, U> T getEnumByWhat(U byWhat, Class<T> clazz, String getWhat) {
        if (Objects.isNull(byWhat)) {
            return null;
        }
        //eg:getCode()
        String get = GET + getWhat;
        Method values = clazz.getMethod(VALUES);
        Method getCode = clazz.getMethod(get);
        //获取所有字段
        Object invokeValues = values.invoke(null);
        for (int i = 0; i < Array.getLength(invokeValues); i++) {
            T enumValue = (T) Array.get(invokeValues, i);
            Object codeGet = getCode.invoke(enumValue);
            if (Objects.equals(codeGet, byWhat)) {
                return enumValue;
            }
        }
        return null;
    }
}
