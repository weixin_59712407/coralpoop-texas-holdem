package org.coralpoop.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.coralpoop.domain.game.model.entity.PlayerEntity;
import org.coralpoop.domain.game.model.entity.PokerEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Configuration
public class GuavaConfig {
    @Bean(name = "pokerCache")
    public Cache<String, Map<String, PokerEntity>> pokerCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(60, TimeUnit.MINUTES)
                .build();
    }

    @Bean(name = "playerCache")
    public Cache<String, Map<String, PlayerEntity>> playerCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build();
    }

    @Bean(name = "handPokerCache")
    public Cache<String, Map<String, Set<PokerEntity>>> handPokerCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build();
    }

    @Bean(name = "publicPokerCache")
    public Cache<String, Set<PokerEntity>> publicPokerCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build();
    }

    @Bean(name = "betPoolCache")
    public Cache<String, Map<Integer,Map<String,Long>>> betPoolCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build();
    }
}