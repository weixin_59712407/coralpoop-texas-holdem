package org.coralpoop.test;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.Cache;
import lombok.extern.slf4j.Slf4j;
import org.coralpoop.domain.game.adapter.repository.IInitGameCacheRepository;
import org.coralpoop.domain.game.model.entity.PlayerEntity;
import org.coralpoop.domain.game.model.entity.PokerEntity;
import org.coralpoop.domain.game.service.init.IInitGame;
import org.coralpoop.types.common.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class InitTest {
    @Resource
    private IInitGameCacheRepository initGameCacheRepository;

    @Resource
    private IInitGame initGame;

    @Test
    public void initPokerTest(){
        String uuid = String.valueOf(UUID.randomUUID());
        initGame.initPoker(uuid);
        Map<String, PokerEntity> map = initGameCacheRepository.pokerCacheGet(uuid);
        if (map != null) {
            log.info("poker uuid:{}, map:{}, size:{}",uuid, JSON.toJSON(map),map.size());
        }
    }

    @Test
    public void initPlayerTest(){
        String uuid = String.valueOf(UUID.randomUUID());
        initGame.initPlayer(uuid,"user001", 1000L);
        initGame.initPlayer(uuid,"user002", 1500L);
        initGame.initPlayer(uuid,"user003", 3000L);
        Map<String, PlayerEntity> map = initGameCacheRepository.playerCacheGet(uuid);
        if (map != null) {
            log.info("player uuid:{}, map:{}, size:{}",uuid, JSON.toJSON(map),map.size());
        }
    }

    @Test
    public void initHandPokerTest(){
        String uuid = String.valueOf(UUID.randomUUID());
        initGame.initPlayer(uuid,"user001", 1000L);
        initGame.initPlayer(uuid,"user002", 1500L);
        initGame.initPlayer(uuid,"user003", 3000L);

        initGame.initHandPoker(uuid);

        Map<String, Set<PokerEntity>> map = initGameCacheRepository.handPokerCacheGet(uuid);
        if (map != null) {
            log.info("hand poker uuid:{}, map:{}, size:{}",uuid, JSON.toJSON(map),map.size());
        }
    }

    @Test
    public void initPublicPokerTest(){
        String uuid = String.valueOf(UUID.randomUUID());
        initGame.initPublicPoker(uuid);
        Set<PokerEntity> set = initGameCacheRepository.publicPokerCacheGet(uuid);
        if (set != null) {
            log.info("public poker uuid:{}, map:{}, size:{}",uuid, JSON.toJSON(set),set.size());
        }
    }

    @Test
    public void initBetPoolTest(){
        String uuid = String.valueOf(UUID.randomUUID());
        initGame.initPlayer(uuid,"user001", 1000L);
        initGame.initPlayer(uuid,"user002", 1500L);
        initGame.initPlayer(uuid,"user003", 3000L);

        initGame.initBetPool(uuid);

        Map<Integer, Map<String, Long>> map = initGameCacheRepository.betPoolCacheGet(uuid);
        if (map != null) {
            log.info("bet pool uuid:{}, map:{}, size:{}",uuid, JSON.toJSON(map),map.size());
        }
    }
}
